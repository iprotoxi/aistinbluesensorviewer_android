﻿Type=StaticCode
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
End Sub

Sub parseHexToFloat (data As String) As Float ' this is for the 8.8 type data
	Dim tempVal As Int
'	tempVal = Bit.ParseInt(data.SubString2(0,2), 16) * 256 + Bit.ParseInt(data.SubString2(2,4), 16)
	tempVal = Bit.ParseInt(data.SubString2(0,4), 16)
	If tempVal >= 32768 Then tempVal = tempVal - 65536
	Dim floatVal As Float
	floatVal = tempVal/256
	Return floatVal
	'Return Round2(floatVal, 2)
End Sub

Sub parseHexToFloat2 (data As String) As Float
	Dim tempVal As Int
	tempVal = Bit.ParseInt(data, 16) * 16
	If tempVal >= 32768 Then tempVal = tempVal - 65536
	Dim floatVal As Float
	'floatVal = tempVal/256
	floatVal = tempVal / 16384 '2^14, in G's
	Return floatVal
	'Return Round2(floatVal, 2)
End Sub

Sub parseHexToFloat4 (data As String) As Float
	Dim tempVal As Int
	tempVal = Bit.ParseInt(data.SubString2(0,3), 16) * 16
	If tempVal >= 32768 Then tempVal = tempVal - 65536
	Dim floatVal As Float
	floatVal = tempVal/256
	Return floatVal
	'Return Round2(floatVal, 2)
End Sub

Sub parseHexToAirPressure (data As String) As Float
	Dim val0, val1, val2 As Long
	val0 = Bit.ParseInt(data.SubString2(0,2), 16)
	val1 = Bit.ParseInt(data.SubString2(2,4), 16)
	val2 = Bit.ParseInt(data.SubString2(4,6), 16)
	Dim floatVal As Float
	floatVal = val0*16 + val1/16 + val2/4096
	Dim returnval As Float = Round2(floatVal, 2)
	Return returnval
End Sub

Sub parseHexToAirPressure2 (data As String) As Float
	Dim val0, val1, val2 As Long
	val0 = Bit.ParseInt(data.SubString2(0,2), 16)
	val1 = Bit.ParseInt(data.SubString2(2,4), 16)

	val2 = Bit.ParseInt(data.SubString2(0,4), 16)

	Dim floatVal As Float
'	floatVal = val0*16 + val1/16 '+ val2/4096
	floatVal = val2/32
	Dim returnval As Float = Round2(floatVal, 2)
	Return returnval
End Sub

Sub ParseHexToString(dataHex As String) As String
	Dim	xHex As String = dataHex.SubString2(0, 4)
	Dim	yHex As String = dataHex.SubString2(4, 8)
	Dim	zHex As String = dataHex.SubString2(8, 12)
	Dim retval As String = 	parseHexToFloat(xHex) & ";" & parseHexToFloat(yHex) & ";" & parseHexToFloat(zHex)
	Return retval
End Sub

Sub ParseResultToString (data As Float) As String
	Dim result As Double = Round2(data, 1)
	Dim decimal As Boolean = IsDecimal(result)
	Select decimal
		Case False
			Return result
		Case Else
			Return result & ".0"
	End Select
End Sub

Sub ParseResultToString2(data As String) As String '3-axis result to csv string
'	Log("data "&data)
	Dim semicolonSeparator1 As Int = data.IndexOf(";")
	Dim semicolonSeparator2 As Int = data.LastIndexOf(";")
	Dim val1 As String = data.SubString2(0, semicolonSeparator1)
	Dim val2 As String = data.SubString2(semicolonSeparator1+1, semicolonSeparator2)
	Dim val3 As String = data.SubString2(semicolonSeparator2+1, data.Length)
	Dim retval As String = ParseResultToString(val1) & ";" & ParseResultToString(val2) & ";" & ParseResultToString(val3)
	Return retval
End Sub

Sub IsDecimal (s As String) As Boolean
	Return IsNumber(s) And Floor(s) = s	
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub twosCompliment(input As String) As Int
	Dim val As Int = Bit.ParseInt(input, 16) * 16
	If val > 32767 Then val = val - 65536
	Return val
End Sub

Sub parseAccelerationToReadable (axisData As String) As Float
	Dim val As Int = twosCompliment(axisData)
	Dim g As Float = (val / 32768 ) * 2 '2|4|8|16
	Return g
End Sub

Sub parseMagnetismToReadable (axisData As String) As Float
	Dim val As Int = twosCompliment(axisData)
	Dim gauss As Float = (val / 32768) * 4 '4|8|12|16
	Return gauss
End Sub

Sub parseRotationToReadable(axisData As String) As Float
	Dim val As Int = twosCompliment(axisData)
	Dim dps As Float = (val / 32768) * 245 '245|500|2000
	If (True) Then
		Return dps 'degrees per second
	Else
		Return dps / 360 'revolutions per second
	End If
End Sub

Sub AccelerationToCSV(threeAxisData As String) As String
	Dim	x As Float = parseAccelerationToReadable(threeAxisData.SubString2(0, 3))
	Dim	y As Float = parseAccelerationToReadable(threeAxisData.SubString2(3, 6))
	Dim	z As Float = parseAccelerationToReadable(threeAxisData.SubString2(6, 9))
	Return x &";"& y &";"& z
End Sub

Sub MagnetismToCSV(threeAxisData As String) As String
	Dim	x As Float = parseMagnetismToReadable(threeAxisData.SubString2(0, 3))
	Dim	y As Float = parseMagnetismToReadable(threeAxisData.SubString2(3, 6))
	Dim	z As Float = parseMagnetismToReadable(threeAxisData.SubString2(6, 9))
	Return x &";"& y &";"& z
End Sub

Sub RotationToCSV(threeAxisData As String) As String
	Dim	x As Float = parseRotationToReadable(threeAxisData.SubString2(0, 3))
	Dim	y As Float = parseRotationToReadable(threeAxisData.SubString2(3, 6))
	Dim	z As Float = parseRotationToReadable(threeAxisData.SubString2(6, 9))
	Return x &";"& y &";"& z
End Sub