﻿Type=Service
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Service Attributes 
	#StartAtBoot: False
	#ExcludeFromLibrary: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.	
	Dim AppData As KeyValueStore
	Dim lastConnectedMAC As String
	Dim lastConnectedName As String
	Dim i As Int = 0
'	Dim autoConnect As Boolean
	
	Dim logFlag As Boolean = True
	Dim SQLstore As Boolean = False
	Dim CSVstore As Boolean
	
	Dim FileName As String
	Dim FileDir As String
	Dim Data As List
	Dim FileOutput As OutputStream
	Dim tw As TextWriter
End Sub

Sub Service_Create
	'This is the program entry point.
	'This is a good place to load resources that are not specific to a single activity.
	Dim database As String =  "aistin.dat"
	Try
		AppData.Initialize(File.DirInternal, database) 'creates a new SQLite database to applications internal storage that saves private data
	Catch
		Log("Service_Create Error: " & LastException)
	End Try
	DateTime.DateFormat = "yyyy-MM-dd HH:mm:ss"
End Sub

Sub Service_Start (StartingIntent As Intent)
	If AppData.ContainsKey("name") Then 
		Try
			lastConnectedName = AppData.GetSimple("name")
		Catch
			Log(LastException)
			AppData.PutSimple("name", "")			
		End Try
		Try
			lastConnectedMAC = AppData.GetSimple("MAC")
		Catch
			Log(LastException)
			AppData.PutSimple("MAC", "")			
		End Try		
	Else
		AppData.PutSimple("MAC", "")
		AppData.PutSimple("name", "")
	End If
End Sub

Sub Service_Destroy
	StopService(BLE2Interface)	
	CallSubDelayed(Me, "CloseTextwriter")
End Sub


Sub StoreData(sdata As String)
'	Log("SQL " & i & ": " & sdata )
	Try
		AppData.PutSimple(i, sdata)
	Catch
		Log("StoreData Error: " & LastException)
	End Try
	i = i + 1
End Sub

Sub CreateLogFile
	DateTime.DateFormat = "yyyy-MM-dd-HH:mm:ss"
	If (logFlag) Then
		If (File.ExternalWritable) Then
			Try
				twClose_
			Catch
				Log("CloseTextwriter: " & LastException)
			End Try
			CSVstore = True
			FileDir = File.DirDefaultExternal
			Log("FileDir: " & FileDir)
			FileName = "log-" & DateTime.Date(DateTime.Now) & ".csv"
			Log("FileName: " & FileName)
			FileOutput = File.OpenOutput(FileDir, FileName, True)
			tw.Initialize(FileOutput)
			Data.Initialize
			If (tw.IsInitialized) Then
				Log("tw.IsInitialized: " & tw.IsInitialized)
			Else
				CSVstore = False
				ToastMessageShow("Cannot log data", False)
			End If
			If (Data.IsInitialized) Then
				Log("Data.IsInitialized: " & Data.IsInitialized)
			Else
				CSVstore = False
				ToastMessageShow("Cannot log data", False)
			End If
			'WriteDataToFile("date;time;sequence;register;data")
		Else
			CSVstore = False
		End If
	End If	
	DateTime.DateFormat = "yyyy-MM-dd HH:mm:ss"	
	DateTime.TimeFormat = "yyyy-MM-dd;HH:mm:ss"	
	Log("CSVstore: " & CSVstore)
	ToastMessageShow("LOGGING DATA" & CRLF & CRLF & "Path: " & CRLF & FileDir & CRLF & CRLF & "File: " & CRLF & FileName , True)
End Sub

Sub WriteDataToFile(sdata As String)
	If (CSVstore) Then
		tw.WriteLine(sdata)
	End If
End Sub

Sub CloseTextwriter
	If (CSVstore) Then	
		CallSubDelayed(Me, "twClose_")
	End If		
End Sub

Sub twClose_
	tw.Close
End Sub