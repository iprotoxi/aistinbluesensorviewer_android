﻿Type=StaticCode
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

	Dim BlueConnectedStatus As String = "---"
	Dim RefreshButtonStatus As String = "Scan"
	Dim ConnectedDeviceName As String = "Aistin Blue"
	
	Dim currentAirTemp As String = "---"
	Dim currentAirHumidity As String = "---"
	Dim currentAirPressure As String = "---"
	Dim currentAcceleration As String = "---"
	Dim currentRotation As String = "---"
	Dim currentMagnetism As String = "---"
	Dim currentBattery As String = "---"	
	
	Dim currentAccX As String = "---"
	Dim currentAccY As String = "---"
	Dim currentAccZ As String = "---"
	Dim currentMagX As String = "---"
	Dim currentMagY As String = "---"
	Dim currentMagZ As String = "---"
	Dim currentGyroX As String = "---"
	Dim currentGyroY As String = "---" 
	Dim currentGyroZ As String = "---"
End Sub