package com.iprotoxi.blue;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class parsedata {
private static parsedata mostCurrent = new parsedata();
public static Object getObject() {
    throw new RuntimeException("Code module does not support this method.");
}
 public anywheresoftware.b4a.keywords.Common __c = null;
public com.iprotoxi.blue.main _main = null;
public com.iprotoxi.blue.bleinterface _bleinterface = null;
public com.iprotoxi.blue.sensordata _sensordata = null;
public com.iprotoxi.blue.starter _starter = null;
public com.iprotoxi.blue.ble2interface _ble2interface = null;
public static String  _accelerationtocsv(anywheresoftware.b4a.BA _ba,String _threeaxisdata) throws Exception{
float _x = 0f;
float _y = 0f;
float _z = 0f;
 //BA.debugLineNum = 129;BA.debugLine="Sub AccelerationToCSV(threeAxisData As String) As";
 //BA.debugLineNum = 130;BA.debugLine="Dim	x As Float = parseAccelerationToReadable(thre";
_x = _parseaccelerationtoreadable(_ba,_threeaxisdata.substring((int) (0),(int) (3)));
 //BA.debugLineNum = 131;BA.debugLine="Dim	y As Float = parseAccelerationToReadable(thre";
_y = _parseaccelerationtoreadable(_ba,_threeaxisdata.substring((int) (3),(int) (6)));
 //BA.debugLineNum = 132;BA.debugLine="Dim	z As Float = parseAccelerationToReadable(thre";
_z = _parseaccelerationtoreadable(_ba,_threeaxisdata.substring((int) (6),(int) (9)));
 //BA.debugLineNum = 133;BA.debugLine="Return x &\";\"& y &\";\"& z";
if (true) return BA.NumberToString(_x)+";"+BA.NumberToString(_y)+";"+BA.NumberToString(_z);
 //BA.debugLineNum = 134;BA.debugLine="End Sub";
return "";
}
public static boolean  _isdecimal(anywheresoftware.b4a.BA _ba,String _s) throws Exception{
 //BA.debugLineNum = 95;BA.debugLine="Sub IsDecimal (s As String) As Boolean";
 //BA.debugLineNum = 96;BA.debugLine="Return IsNumber(s) And Floor(s) = s";
if (true) return anywheresoftware.b4a.keywords.Common.IsNumber(_s) && anywheresoftware.b4a.keywords.Common.Floor((double)(Double.parseDouble(_s)))==(double)(Double.parseDouble(_s));
 //BA.debugLineNum = 97;BA.debugLine="End Sub";
return false;
}
public static String  _magnetismtocsv(anywheresoftware.b4a.BA _ba,String _threeaxisdata) throws Exception{
float _x = 0f;
float _y = 0f;
float _z = 0f;
 //BA.debugLineNum = 136;BA.debugLine="Sub MagnetismToCSV(threeAxisData As String) As Str";
 //BA.debugLineNum = 137;BA.debugLine="Dim	x As Float = parseMagnetismToReadable(threeAx";
_x = _parsemagnetismtoreadable(_ba,_threeaxisdata.substring((int) (0),(int) (3)));
 //BA.debugLineNum = 138;BA.debugLine="Dim	y As Float = parseMagnetismToReadable(threeAx";
_y = _parsemagnetismtoreadable(_ba,_threeaxisdata.substring((int) (3),(int) (6)));
 //BA.debugLineNum = 139;BA.debugLine="Dim	z As Float = parseMagnetismToReadable(threeAx";
_z = _parsemagnetismtoreadable(_ba,_threeaxisdata.substring((int) (6),(int) (9)));
 //BA.debugLineNum = 140;BA.debugLine="Return x &\";\"& y &\";\"& z";
if (true) return BA.NumberToString(_x)+";"+BA.NumberToString(_y)+";"+BA.NumberToString(_z);
 //BA.debugLineNum = 141;BA.debugLine="End Sub";
return "";
}
public static float  _parseaccelerationtoreadable(anywheresoftware.b4a.BA _ba,String _axisdata) throws Exception{
int _val = 0;
float _g = 0f;
 //BA.debugLineNum = 107;BA.debugLine="Sub parseAccelerationToReadable (axisData As Strin";
 //BA.debugLineNum = 108;BA.debugLine="Dim val As Int = twosCompliment(axisData)";
_val = _twoscompliment(_ba,_axisdata);
 //BA.debugLineNum = 109;BA.debugLine="Dim g As Float = (val / 32768 ) * 2 '2|4|8|16";
_g = (float) ((_val/(double)32768)*2);
 //BA.debugLineNum = 110;BA.debugLine="Return g";
if (true) return _g;
 //BA.debugLineNum = 111;BA.debugLine="End Sub";
return 0f;
}
public static float  _parsehextoairpressure(anywheresoftware.b4a.BA _ba,String _data) throws Exception{
long _val0 = 0L;
long _val1 = 0L;
long _val2 = 0L;
float _floatval = 0f;
float _returnval = 0f;
 //BA.debugLineNum = 40;BA.debugLine="Sub parseHexToAirPressure (data As String) As Floa";
 //BA.debugLineNum = 41;BA.debugLine="Dim val0, val1, val2 As Long";
_val0 = 0L;
_val1 = 0L;
_val2 = 0L;
 //BA.debugLineNum = 42;BA.debugLine="val0 = Bit.ParseInt(data.SubString2(0,2), 16)";
_val0 = (long) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (0),(int) (2)),(int) (16)));
 //BA.debugLineNum = 43;BA.debugLine="val1 = Bit.ParseInt(data.SubString2(2,4), 16)";
_val1 = (long) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (2),(int) (4)),(int) (16)));
 //BA.debugLineNum = 44;BA.debugLine="val2 = Bit.ParseInt(data.SubString2(4,6), 16)";
_val2 = (long) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (4),(int) (6)),(int) (16)));
 //BA.debugLineNum = 45;BA.debugLine="Dim floatVal As Float";
_floatval = 0f;
 //BA.debugLineNum = 46;BA.debugLine="floatVal = val0*16 + val1/16 + val2/4096";
_floatval = (float) (_val0*16+_val1/(double)16+_val2/(double)4096);
 //BA.debugLineNum = 47;BA.debugLine="Dim returnval As Float = Round2(floatVal, 2)";
_returnval = (float) (anywheresoftware.b4a.keywords.Common.Round2(_floatval,(int) (2)));
 //BA.debugLineNum = 48;BA.debugLine="Return returnval";
if (true) return _returnval;
 //BA.debugLineNum = 49;BA.debugLine="End Sub";
return 0f;
}
public static float  _parsehextoairpressure2(anywheresoftware.b4a.BA _ba,String _data) throws Exception{
long _val0 = 0L;
long _val1 = 0L;
long _val2 = 0L;
float _floatval = 0f;
float _returnval = 0f;
 //BA.debugLineNum = 51;BA.debugLine="Sub parseHexToAirPressure2 (data As String) As Flo";
 //BA.debugLineNum = 52;BA.debugLine="Dim val0, val1, val2 As Long";
_val0 = 0L;
_val1 = 0L;
_val2 = 0L;
 //BA.debugLineNum = 53;BA.debugLine="val0 = Bit.ParseInt(data.SubString2(0,2), 16)";
_val0 = (long) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (0),(int) (2)),(int) (16)));
 //BA.debugLineNum = 54;BA.debugLine="val1 = Bit.ParseInt(data.SubString2(2,4), 16)";
_val1 = (long) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (2),(int) (4)),(int) (16)));
 //BA.debugLineNum = 56;BA.debugLine="val2 = Bit.ParseInt(data.SubString2(0,4), 16)";
_val2 = (long) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (0),(int) (4)),(int) (16)));
 //BA.debugLineNum = 58;BA.debugLine="Dim floatVal As Float";
_floatval = 0f;
 //BA.debugLineNum = 60;BA.debugLine="floatVal = val2/32";
_floatval = (float) (_val2/(double)32);
 //BA.debugLineNum = 61;BA.debugLine="Dim returnval As Float = Round2(floatVal, 2)";
_returnval = (float) (anywheresoftware.b4a.keywords.Common.Round2(_floatval,(int) (2)));
 //BA.debugLineNum = 62;BA.debugLine="Return returnval";
if (true) return _returnval;
 //BA.debugLineNum = 63;BA.debugLine="End Sub";
return 0f;
}
public static float  _parsehextofloat(anywheresoftware.b4a.BA _ba,String _data) throws Exception{
int _tempval = 0;
float _floatval = 0f;
 //BA.debugLineNum = 8;BA.debugLine="Sub parseHexToFloat (data As String) As Float ' th";
 //BA.debugLineNum = 9;BA.debugLine="Dim tempVal As Int";
_tempval = 0;
 //BA.debugLineNum = 11;BA.debugLine="tempVal = Bit.ParseInt(data.SubString2(0,4), 16)";
_tempval = anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (0),(int) (4)),(int) (16));
 //BA.debugLineNum = 12;BA.debugLine="If tempVal >= 32768 Then tempVal = tempVal - 6553";
if (_tempval>=32768) { 
_tempval = (int) (_tempval-65536);};
 //BA.debugLineNum = 13;BA.debugLine="Dim floatVal As Float";
_floatval = 0f;
 //BA.debugLineNum = 14;BA.debugLine="floatVal = tempVal/256";
_floatval = (float) (_tempval/(double)256);
 //BA.debugLineNum = 15;BA.debugLine="Return floatVal";
if (true) return _floatval;
 //BA.debugLineNum = 17;BA.debugLine="End Sub";
return 0f;
}
public static float  _parsehextofloat2(anywheresoftware.b4a.BA _ba,String _data) throws Exception{
int _tempval = 0;
float _floatval = 0f;
 //BA.debugLineNum = 19;BA.debugLine="Sub parseHexToFloat2 (data As String) As Float";
 //BA.debugLineNum = 20;BA.debugLine="Dim tempVal As Int";
_tempval = 0;
 //BA.debugLineNum = 21;BA.debugLine="tempVal = Bit.ParseInt(data, 16) * 16";
_tempval = (int) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data,(int) (16))*16);
 //BA.debugLineNum = 22;BA.debugLine="If tempVal >= 32768 Then tempVal = tempVal - 6553";
if (_tempval>=32768) { 
_tempval = (int) (_tempval-65536);};
 //BA.debugLineNum = 23;BA.debugLine="Dim floatVal As Float";
_floatval = 0f;
 //BA.debugLineNum = 25;BA.debugLine="floatVal = tempVal / 16384 '2^14, in G's";
_floatval = (float) (_tempval/(double)16384);
 //BA.debugLineNum = 26;BA.debugLine="Return floatVal";
if (true) return _floatval;
 //BA.debugLineNum = 28;BA.debugLine="End Sub";
return 0f;
}
public static float  _parsehextofloat4(anywheresoftware.b4a.BA _ba,String _data) throws Exception{
int _tempval = 0;
float _floatval = 0f;
 //BA.debugLineNum = 30;BA.debugLine="Sub parseHexToFloat4 (data As String) As Float";
 //BA.debugLineNum = 31;BA.debugLine="Dim tempVal As Int";
_tempval = 0;
 //BA.debugLineNum = 32;BA.debugLine="tempVal = Bit.ParseInt(data.SubString2(0,3), 16)";
_tempval = (int) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_data.substring((int) (0),(int) (3)),(int) (16))*16);
 //BA.debugLineNum = 33;BA.debugLine="If tempVal >= 32768 Then tempVal = tempVal - 6553";
if (_tempval>=32768) { 
_tempval = (int) (_tempval-65536);};
 //BA.debugLineNum = 34;BA.debugLine="Dim floatVal As Float";
_floatval = 0f;
 //BA.debugLineNum = 35;BA.debugLine="floatVal = tempVal/256";
_floatval = (float) (_tempval/(double)256);
 //BA.debugLineNum = 36;BA.debugLine="Return floatVal";
if (true) return _floatval;
 //BA.debugLineNum = 38;BA.debugLine="End Sub";
return 0f;
}
public static String  _parsehextostring(anywheresoftware.b4a.BA _ba,String _datahex) throws Exception{
String _xhex = "";
String _yhex = "";
String _zhex = "";
String _retval = "";
 //BA.debugLineNum = 65;BA.debugLine="Sub ParseHexToString(dataHex As String) As String";
 //BA.debugLineNum = 66;BA.debugLine="Dim	xHex As String = dataHex.SubString2(0, 4)";
_xhex = _datahex.substring((int) (0),(int) (4));
 //BA.debugLineNum = 67;BA.debugLine="Dim	yHex As String = dataHex.SubString2(4, 8)";
_yhex = _datahex.substring((int) (4),(int) (8));
 //BA.debugLineNum = 68;BA.debugLine="Dim	zHex As String = dataHex.SubString2(8, 12)";
_zhex = _datahex.substring((int) (8),(int) (12));
 //BA.debugLineNum = 69;BA.debugLine="Dim retval As String = 	parseHexToFloat(xHex) & \"";
_retval = BA.NumberToString(_parsehextofloat(_ba,_xhex))+";"+BA.NumberToString(_parsehextofloat(_ba,_yhex))+";"+BA.NumberToString(_parsehextofloat(_ba,_zhex));
 //BA.debugLineNum = 70;BA.debugLine="Return retval";
if (true) return _retval;
 //BA.debugLineNum = 71;BA.debugLine="End Sub";
return "";
}
public static float  _parsemagnetismtoreadable(anywheresoftware.b4a.BA _ba,String _axisdata) throws Exception{
int _val = 0;
float _gauss = 0f;
 //BA.debugLineNum = 113;BA.debugLine="Sub parseMagnetismToReadable (axisData As String)";
 //BA.debugLineNum = 114;BA.debugLine="Dim val As Int = twosCompliment(axisData)";
_val = _twoscompliment(_ba,_axisdata);
 //BA.debugLineNum = 115;BA.debugLine="Dim gauss As Float = (val / 32768) * 4 '4|8|12|16";
_gauss = (float) ((_val/(double)32768)*4);
 //BA.debugLineNum = 116;BA.debugLine="Return gauss";
if (true) return _gauss;
 //BA.debugLineNum = 117;BA.debugLine="End Sub";
return 0f;
}
public static String  _parseresulttostring(anywheresoftware.b4a.BA _ba,float _data) throws Exception{
double _result = 0;
boolean _decimal = false;
 //BA.debugLineNum = 73;BA.debugLine="Sub ParseResultToString (data As Float) As String";
 //BA.debugLineNum = 74;BA.debugLine="Dim result As Double = Round2(data, 1)";
_result = anywheresoftware.b4a.keywords.Common.Round2(_data,(int) (1));
 //BA.debugLineNum = 75;BA.debugLine="Dim decimal As Boolean = IsDecimal(result)";
_decimal = _isdecimal(_ba,BA.NumberToString(_result));
 //BA.debugLineNum = 76;BA.debugLine="Select decimal";
switch (BA.switchObjectToInt(_decimal,anywheresoftware.b4a.keywords.Common.False)) {
case 0:
 //BA.debugLineNum = 78;BA.debugLine="Return result";
if (true) return BA.NumberToString(_result);
 break;
default:
 //BA.debugLineNum = 80;BA.debugLine="Return result & \".0\"";
if (true) return BA.NumberToString(_result)+".0";
 break;
}
;
 //BA.debugLineNum = 82;BA.debugLine="End Sub";
return "";
}
public static String  _parseresulttostring2(anywheresoftware.b4a.BA _ba,String _data) throws Exception{
int _semicolonseparator1 = 0;
int _semicolonseparator2 = 0;
String _val1 = "";
String _val2 = "";
String _val3 = "";
String _retval = "";
 //BA.debugLineNum = 84;BA.debugLine="Sub ParseResultToString2(data As String) As String";
 //BA.debugLineNum = 86;BA.debugLine="Dim semicolonSeparator1 As Int = data.IndexOf(\";\"";
_semicolonseparator1 = _data.indexOf(";");
 //BA.debugLineNum = 87;BA.debugLine="Dim semicolonSeparator2 As Int = data.LastIndexOf";
_semicolonseparator2 = _data.lastIndexOf(";");
 //BA.debugLineNum = 88;BA.debugLine="Dim val1 As String = data.SubString2(0, semicolon";
_val1 = _data.substring((int) (0),_semicolonseparator1);
 //BA.debugLineNum = 89;BA.debugLine="Dim val2 As String = data.SubString2(semicolonSep";
_val2 = _data.substring((int) (_semicolonseparator1+1),_semicolonseparator2);
 //BA.debugLineNum = 90;BA.debugLine="Dim val3 As String = data.SubString2(semicolonSep";
_val3 = _data.substring((int) (_semicolonseparator2+1),_data.length());
 //BA.debugLineNum = 91;BA.debugLine="Dim retval As String = ParseResultToString(val1)";
_retval = _parseresulttostring(_ba,(float)(Double.parseDouble(_val1)))+";"+_parseresulttostring(_ba,(float)(Double.parseDouble(_val2)))+";"+_parseresulttostring(_ba,(float)(Double.parseDouble(_val3)));
 //BA.debugLineNum = 92;BA.debugLine="Return retval";
if (true) return _retval;
 //BA.debugLineNum = 93;BA.debugLine="End Sub";
return "";
}
public static float  _parserotationtoreadable(anywheresoftware.b4a.BA _ba,String _axisdata) throws Exception{
int _val = 0;
float _dps = 0f;
 //BA.debugLineNum = 119;BA.debugLine="Sub parseRotationToReadable(axisData As String) As";
 //BA.debugLineNum = 120;BA.debugLine="Dim val As Int = twosCompliment(axisData)";
_val = _twoscompliment(_ba,_axisdata);
 //BA.debugLineNum = 121;BA.debugLine="Dim dps As Float = (val / 32768) * 245 '245|500|2";
_dps = (float) ((_val/(double)32768)*245);
 //BA.debugLineNum = 122;BA.debugLine="If (True) Then";
if ((anywheresoftware.b4a.keywords.Common.True)) { 
 //BA.debugLineNum = 123;BA.debugLine="Return dps 'degrees per second";
if (true) return _dps;
 }else {
 //BA.debugLineNum = 125;BA.debugLine="Return dps / 360 'revolutions per second";
if (true) return (float) (_dps/(double)360);
 };
 //BA.debugLineNum = 127;BA.debugLine="End Sub";
return 0f;
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 3;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return "";
}
public static String  _rotationtocsv(anywheresoftware.b4a.BA _ba,String _threeaxisdata) throws Exception{
float _x = 0f;
float _y = 0f;
float _z = 0f;
 //BA.debugLineNum = 143;BA.debugLine="Sub RotationToCSV(threeAxisData As String) As Stri";
 //BA.debugLineNum = 144;BA.debugLine="Dim	x As Float = parseRotationToReadable(threeAxi";
_x = _parserotationtoreadable(_ba,_threeaxisdata.substring((int) (0),(int) (3)));
 //BA.debugLineNum = 145;BA.debugLine="Dim	y As Float = parseRotationToReadable(threeAxi";
_y = _parserotationtoreadable(_ba,_threeaxisdata.substring((int) (3),(int) (6)));
 //BA.debugLineNum = 146;BA.debugLine="Dim	z As Float = parseRotationToReadable(threeAxi";
_z = _parserotationtoreadable(_ba,_threeaxisdata.substring((int) (6),(int) (9)));
 //BA.debugLineNum = 147;BA.debugLine="Return x &\";\"& y &\";\"& z";
if (true) return BA.NumberToString(_x)+";"+BA.NumberToString(_y)+";"+BA.NumberToString(_z);
 //BA.debugLineNum = 148;BA.debugLine="End Sub";
return "";
}
public static int  _twoscompliment(anywheresoftware.b4a.BA _ba,String _input) throws Exception{
int _val = 0;
 //BA.debugLineNum = 101;BA.debugLine="Sub twosCompliment(input As String) As Int";
 //BA.debugLineNum = 102;BA.debugLine="Dim val As Int = Bit.ParseInt(input, 16) * 16";
_val = (int) (anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_input,(int) (16))*16);
 //BA.debugLineNum = 103;BA.debugLine="If val > 32767 Then val = val - 65536";
if (_val>32767) { 
_val = (int) (_val-65536);};
 //BA.debugLineNum = 104;BA.debugLine="Return val";
if (true) return _val;
 //BA.debugLineNum = 105;BA.debugLine="End Sub";
return 0;
}
}
