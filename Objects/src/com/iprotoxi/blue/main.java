package com.iprotoxi.blue;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class main extends Activity implements B4AActivity{
	public static main mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = true;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "com.iprotoxi.blue", "com.iprotoxi.blue.main");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (main).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "com.iprotoxi.blue", "com.iprotoxi.blue.main");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "com.iprotoxi.blue.main", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (main) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (main) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (main) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.Timer _uitimer = null;
public static anywheresoftware.b4a.phone.Phone.PhoneWakeState _awake = null;
public static boolean _closedbyuser = false;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewlogo = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelacceleration = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelairhumidity = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelairpressure = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelairtemp = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelbatterylevel = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelblue = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelbluehead = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelmagnetism = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelrotation = null;
public anywheresoftware.b4a.objects.PanelWrapper _paneldatetime = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelacceleration = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelairhumidity = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelairpressure = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelairtemp = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelbatterylevel = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelmagnetism = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelpanelrotation = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelacceleration = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelairhumidity = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelairpressure = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelairtemp = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelbatterylevel = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelblueconnectstatus = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelbluedevice = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelmagnetism = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelrotation = null;
public anywheresoftware.b4a.objects.LabelWrapper _labeldate = null;
public anywheresoftware.b4a.objects.LabelWrapper _labeltime = null;
public anywheresoftware.b4a.objects.ButtonWrapper _buttonrefresh = null;
public com.iprotoxi.blue.bleinterface _bleinterface = null;
public com.iprotoxi.blue.parsedata _parsedata = null;
public com.iprotoxi.blue.sensordata _sensordata = null;
public com.iprotoxi.blue.starter _starter = null;
public com.iprotoxi.blue.ble2interface _ble2interface = null;

public static boolean isAnyActivityVisible() {
    boolean vis = false;
vis = vis | (main.mostCurrent != null);
return vis;}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 72;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 73;BA.debugLine="Activity.LoadLayout(\"Main\")";
mostCurrent._activity.LoadLayout("Main",mostCurrent.activityBA);
 //BA.debugLineNum = 74;BA.debugLine="Activity.AddMenuItem(\"Disconnect device\", \"menuDi";
mostCurrent._activity.AddMenuItem("Disconnect device","menuDisconnectBlue");
 //BA.debugLineNum = 75;BA.debugLine="Activity.AddMenuItem(\"About\", \"menuAbout\")";
mostCurrent._activity.AddMenuItem("About","menuAbout");
 //BA.debugLineNum = 76;BA.debugLine="Activity.AddMenuItem(\"Exit\", \"menuExit\")";
mostCurrent._activity.AddMenuItem("Exit","menuExit");
 //BA.debugLineNum = 77;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 78;BA.debugLine="awake.KeepAlive(True)";
_awake.KeepAlive(processBA,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 79;BA.debugLine="UITimer.Initialize(\"RefreshUI\", 200)";
_uitimer.Initialize(processBA,"RefreshUI",(long) (200));
 };
 //BA.debugLineNum = 81;BA.debugLine="End Sub";
return "";
}
public static boolean  _activity_keypress(int _keycode) throws Exception{
 //BA.debugLineNum = 91;BA.debugLine="Sub Activity_KeyPress (KeyCode As Int) As Boolean";
 //BA.debugLineNum = 92;BA.debugLine="If KeyCode = KeyCodes.KEYCODE_BACK Then";
if (_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_BACK) { 
 //BA.debugLineNum = 93;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 94;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 96;BA.debugLine="Return False";
if (true) return anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 99;BA.debugLine="End Sub";
return false;
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 87;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 88;BA.debugLine="UITimer.Enabled = False";
_uitimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 89;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 83;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 84;BA.debugLine="UITimer.Enabled = True";
_uitimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 85;BA.debugLine="End Sub";
return "";
}
public static String  _buttonrefresh_click() throws Exception{
 //BA.debugLineNum = 170;BA.debugLine="Sub ButtonRefresh_Click";
 //BA.debugLineNum = 171;BA.debugLine="StartService(BLE2Interface)";
anywheresoftware.b4a.keywords.Common.StartService(mostCurrent.activityBA,(Object)(mostCurrent._ble2interface.getObject()));
 //BA.debugLineNum = 172;BA.debugLine="CallSubDelayed(BLE2Interface, \"ScanForDevices\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(mostCurrent.activityBA,(Object)(mostCurrent._ble2interface.getObject()),"ScanForDevices");
 //BA.debugLineNum = 174;BA.debugLine="End Sub";
return "";
}
public static String  _delay(long _nmillisecond,boolean _enableevents) throws Exception{
long _nbegintime = 0L;
long _nendtime = 0L;
 //BA.debugLineNum = 237;BA.debugLine="Sub Delay (nMilliSecond As Long, EnableEvents As B";
 //BA.debugLineNum = 238;BA.debugLine="Dim nBeginTime, nEndTime As Long";
_nbegintime = 0L;
_nendtime = 0L;
 //BA.debugLineNum = 239;BA.debugLine="nEndTime = DateTime.Now + nMilliSecond";
_nendtime = (long) (anywheresoftware.b4a.keywords.Common.DateTime.getNow()+_nmillisecond);
 //BA.debugLineNum = 240;BA.debugLine="nBeginTime = DateTime.Now";
_nbegintime = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 241;BA.debugLine="Do While nBeginTime < nEndTime";
while (_nbegintime<_nendtime) {
 //BA.debugLineNum = 242;BA.debugLine="nBeginTime = DateTime.Now";
_nbegintime = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 243;BA.debugLine="If nEndTime < nBeginTime Then Return";
if (_nendtime<_nbegintime) { 
if (true) return "";};
 //BA.debugLineNum = 244;BA.debugLine="If (EnableEvents) Then DoEvents";
if ((_enableevents)) { 
anywheresoftware.b4a.keywords.Common.DoEvents();};
 }
;
 //BA.debugLineNum = 246;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 34;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 37;BA.debugLine="Private ImageViewLogo As ImageView";
mostCurrent._imageviewlogo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private PanelAcceleration As Panel";
mostCurrent._panelacceleration = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private PanelAirHumidity As Panel";
mostCurrent._panelairhumidity = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private PanelAirPressure As Panel";
mostCurrent._panelairpressure = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private PanelAirTemp As Panel";
mostCurrent._panelairtemp = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private PanelBatteryLevel As Panel";
mostCurrent._panelbatterylevel = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Private PanelBlue As Panel";
mostCurrent._panelblue = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private PanelBlueHead As Panel";
mostCurrent._panelbluehead = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 46;BA.debugLine="Private PanelMagnetism As Panel";
mostCurrent._panelmagnetism = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private PanelRotation As Panel";
mostCurrent._panelrotation = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 48;BA.debugLine="Private PanelDatetime As Panel";
mostCurrent._paneldatetime = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 50;BA.debugLine="Private LabelPanelAcceleration As Label";
mostCurrent._labelpanelacceleration = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 51;BA.debugLine="Private LabelPanelAirHumidity As Label";
mostCurrent._labelpanelairhumidity = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 52;BA.debugLine="Private LabelPanelAirPressure As Label";
mostCurrent._labelpanelairpressure = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 53;BA.debugLine="Private LabelPanelAirTemp As Label";
mostCurrent._labelpanelairtemp = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 54;BA.debugLine="Private LabelPanelBatteryLevel As Label";
mostCurrent._labelpanelbatterylevel = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 55;BA.debugLine="Private LabelPanelMagnetism As Label";
mostCurrent._labelpanelmagnetism = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 56;BA.debugLine="Private LabelPanelRotation As Label";
mostCurrent._labelpanelrotation = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 57;BA.debugLine="Private LabelAcceleration As Label";
mostCurrent._labelacceleration = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 58;BA.debugLine="Private LabelAirHumidity As Label";
mostCurrent._labelairhumidity = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 59;BA.debugLine="Private LabelAirPressure As Label";
mostCurrent._labelairpressure = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 60;BA.debugLine="Private LabelAirTemp As Label";
mostCurrent._labelairtemp = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 61;BA.debugLine="Private LabelBatteryLevel As Label";
mostCurrent._labelbatterylevel = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 62;BA.debugLine="Private LabelBlueConnectStatus As Label";
mostCurrent._labelblueconnectstatus = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 63;BA.debugLine="Private LabelBlueDevice As Label";
mostCurrent._labelbluedevice = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 64;BA.debugLine="Private LabelMagnetism As Label";
mostCurrent._labelmagnetism = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 65;BA.debugLine="Private LabelRotation As Label";
mostCurrent._labelrotation = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 66;BA.debugLine="Private LabelDate As Label";
mostCurrent._labeldate = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 67;BA.debugLine="Private LabelTime As Label";
mostCurrent._labeltime = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 69;BA.debugLine="Private ButtonRefresh As Button";
mostCurrent._buttonrefresh = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 70;BA.debugLine="End Sub";
return "";
}
public static String  _menuabout_click() throws Exception{
String _logpath = "";
String _logfile = "";
 //BA.debugLineNum = 108;BA.debugLine="Sub menuAbout_Click";
 //BA.debugLineNum = 109;BA.debugLine="Dim logPath As String = Starter.FileDir";
_logpath = mostCurrent._starter._filedir;
 //BA.debugLineNum = 110;BA.debugLine="Dim logFile As String = Starter.FileName";
_logfile = mostCurrent._starter._filename;
 //BA.debugLineNum = 111;BA.debugLine="Msgbox(	\"Aistin Blue Data Viewer \" & Application.";
anywheresoftware.b4a.keywords.Common.Msgbox("Aistin Blue Data Viewer "+anywheresoftware.b4a.keywords.Common.Application.getVersionName()+anywheresoftware.b4a.keywords.Common.CRLF+""+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"iProtoXi Oy"+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"All sensor data is logged automatically."+anywheresoftware.b4a.keywords.Common.CRLF+"A new log file is created, when device is connected."+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Current Path: "+anywheresoftware.b4a.keywords.Common.CRLF+_logpath+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Current File: "+anywheresoftware.b4a.keywords.Common.CRLF+_logfile+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Log Format: "+anywheresoftware.b4a.keywords.Common.CRLF+"timestamp; sequence; register; data"+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Data in Register ID F1 (BTL3XX): "+anywheresoftware.b4a.keywords.Common.CRLF+"accX; accY; accZ; magX; magY; magZ; gyrX; gyrY; gyrZ"+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Data in Register ID F2 (BTL3X1): "+anywheresoftware.b4a.keywords.Common.CRLF+"barometer; temperature; humidity"+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Data in Register ID F2 (BTL3X2): "+anywheresoftware.b4a.keywords.Common.CRLF+"barometer; temperature"+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF,"About",mostCurrent.activityBA);
 //BA.debugLineNum = 123;BA.debugLine="End Sub";
return "";
}
public static String  _menudisconnectblue_click() throws Exception{
 //BA.debugLineNum = 101;BA.debugLine="Sub menuDisconnectBlue_Click";
 //BA.debugLineNum = 102;BA.debugLine="closedByUser = True";
_closedbyuser = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 103;BA.debugLine="CallSub(BLE2Interface, \"DisconnectBLE\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(mostCurrent.activityBA,(Object)(mostCurrent._ble2interface.getObject()),"DisconnectBLE");
 //BA.debugLineNum = 104;BA.debugLine="CallSub(Starter, \"CloseTextwriter\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(mostCurrent.activityBA,(Object)(mostCurrent._starter.getObject()),"CloseTextwriter");
 //BA.debugLineNum = 106;BA.debugLine="End Sub";
return "";
}
public static String  _menuexit_click() throws Exception{
 //BA.debugLineNum = 125;BA.debugLine="Sub menuExit_Click";
 //BA.debugLineNum = 126;BA.debugLine="closedByUser = True";
_closedbyuser = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 127;BA.debugLine="CallSub(BLE2Interface, \"DisconnectBLE\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(mostCurrent.activityBA,(Object)(mostCurrent._ble2interface.getObject()),"DisconnectBLE");
 //BA.debugLineNum = 128;BA.debugLine="CallSub(Starter, \"CloseTextwriter\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(mostCurrent.activityBA,(Object)(mostCurrent._starter.getObject()),"CloseTextwriter");
 //BA.debugLineNum = 129;BA.debugLine="Delay(1000, True)";
_delay((long) (1000),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 130;BA.debugLine="awake.ReleaseKeepAlive";
_awake.ReleaseKeepAlive();
 //BA.debugLineNum = 131;BA.debugLine="awake.ReleasePartialLock";
_awake.ReleasePartialLock();
 //BA.debugLineNum = 132;BA.debugLine="StopService(BLE2Interface)";
anywheresoftware.b4a.keywords.Common.StopService(mostCurrent.activityBA,(Object)(mostCurrent._ble2interface.getObject()));
 //BA.debugLineNum = 133;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 134;BA.debugLine="ExitApplication";
anywheresoftware.b4a.keywords.Common.ExitApplication();
 //BA.debugLineNum = 135;BA.debugLine="End Sub";
return "";
}
public static String  _panelacceleration_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 193;BA.debugLine="Sub PanelAcceleration_Touch (Action As Int, X As F";
 //BA.debugLineNum = 194;BA.debugLine="End Sub";
return "";
}
public static String  _panelairhumidity_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 191;BA.debugLine="Sub PanelAirHumidity_Touch (Action As Int, X As Fl";
 //BA.debugLineNum = 192;BA.debugLine="End Sub";
return "";
}
public static String  _panelairpressure_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 189;BA.debugLine="Sub PanelAirPressure_Touch (Action As Int, X As Fl";
 //BA.debugLineNum = 190;BA.debugLine="End Sub";
return "";
}
public static String  _panelairtemp_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 187;BA.debugLine="Sub PanelAirTemp_Touch (Action As Int, X As Float,";
 //BA.debugLineNum = 188;BA.debugLine="End Sub";
return "";
}
public static String  _panelbatterylevel_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 185;BA.debugLine="Sub PanelBatteryLevel_Touch (Action As Int, X As F";
 //BA.debugLineNum = 186;BA.debugLine="End Sub";
return "";
}
public static String  _panelblue_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 183;BA.debugLine="Sub PanelBlue_Touch (Action As Int, X As Float, Y";
 //BA.debugLineNum = 184;BA.debugLine="End Sub";
return "";
}
public static String  _panelbluehead_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 181;BA.debugLine="Sub PanelBlueHead_Touch (Action As Int, X As Float";
 //BA.debugLineNum = 182;BA.debugLine="End Sub";
return "";
}
public static String  _paneldatetime_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 195;BA.debugLine="Sub PanelDatetime_Touch (Action As Int, X As Float";
 //BA.debugLineNum = 196;BA.debugLine="End Sub";
return "";
}
public static String  _panelmagnetism_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 179;BA.debugLine="Sub PanelMagnetism_Touch (Action As Int, X As Floa";
 //BA.debugLineNum = 180;BA.debugLine="End Sub";
return "";
}
public static String  _panelrotation_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 177;BA.debugLine="Sub PanelRotation_Touch (Action As Int, X As Float";
 //BA.debugLineNum = 178;BA.debugLine="End Sub";
return "";
}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main._process_globals();
bleinterface._process_globals();
parsedata._process_globals();
sensordata._process_globals();
starter._process_globals();
ble2interface._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 26;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 29;BA.debugLine="Dim UITimer As Timer";
_uitimer = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 30;BA.debugLine="Dim awake As PhoneWakeState";
_awake = new anywheresoftware.b4a.phone.Phone.PhoneWakeState();
 //BA.debugLineNum = 31;BA.debugLine="Dim closedByUser As Boolean = False";
_closedbyuser = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 32;BA.debugLine="End Sub";
return "";
}
public static String  _progressdialoghide_() throws Exception{
 //BA.debugLineNum = 202;BA.debugLine="Sub ProgressDialogHide_";
 //BA.debugLineNum = 203;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 204;BA.debugLine="End Sub";
return "";
}
public static String  _progressdialogshow_(String _text) throws Exception{
 //BA.debugLineNum = 198;BA.debugLine="Sub ProgressDialogShow_(text As String)";
 //BA.debugLineNum = 199;BA.debugLine="ProgressDialogShow(text)";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow(mostCurrent.activityBA,_text);
 //BA.debugLineNum = 200;BA.debugLine="End Sub";
return "";
}
public static String  _refreshui_tick() throws Exception{
String _label1 = "";
String _label2 = "";
String _label3 = "";
String _label4 = "";
String _label5 = "";
String _label6 = "";
String _label7 = "";
String _label8 = "";
 //BA.debugLineNum = 137;BA.debugLine="Sub RefreshUI_Tick";
 //BA.debugLineNum = 138;BA.debugLine="Dim label1 As String = SensorData.currentAirTemp";
_label1 = mostCurrent._sensordata._currentairtemp;
 //BA.debugLineNum = 139;BA.debugLine="Dim label2 As String = SensorData.currentAirHumid";
_label2 = mostCurrent._sensordata._currentairhumidity;
 //BA.debugLineNum = 140;BA.debugLine="Dim label3 As String = SensorData.currentAirPress";
_label3 = mostCurrent._sensordata._currentairpressure;
 //BA.debugLineNum = 142;BA.debugLine="Dim label4 As String = SensorData.currentAccelera";
_label4 = mostCurrent._sensordata._currentacceleration;
 //BA.debugLineNum = 143;BA.debugLine="Dim label5 As String = SensorData.currentMagnetis";
_label5 = mostCurrent._sensordata._currentmagnetism;
 //BA.debugLineNum = 144;BA.debugLine="Dim label6 As String = SensorData.currentRotation";
_label6 = mostCurrent._sensordata._currentrotation;
 //BA.debugLineNum = 146;BA.debugLine="Dim label7 As String = SensorData.currentBattery";
_label7 = mostCurrent._sensordata._currentbattery;
 //BA.debugLineNum = 147;BA.debugLine="If SensorData.ConnectedDeviceName <> Null Then";
if (mostCurrent._sensordata._connecteddevicename!= null) { 
 //BA.debugLineNum = 148;BA.debugLine="Dim label8 As String = SensorData.ConnectedDevic";
_label8 = mostCurrent._sensordata._connecteddevicename;
 }else {
 //BA.debugLineNum = 150;BA.debugLine="Dim label8 As String = \"Aistin Blue\"";
_label8 = "Aistin Blue";
 };
 //BA.debugLineNum = 153;BA.debugLine="LabelAirTemp.Text 		= label1";
mostCurrent._labelairtemp.setText((Object)(_label1));
 //BA.debugLineNum = 154;BA.debugLine="LabelAirHumidity.Text 	= label2";
mostCurrent._labelairhumidity.setText((Object)(_label2));
 //BA.debugLineNum = 155;BA.debugLine="LabelAirPressure.Text 	= label3";
mostCurrent._labelairpressure.setText((Object)(_label3));
 //BA.debugLineNum = 156;BA.debugLine="LabelAcceleration.Text 	= label4";
mostCurrent._labelacceleration.setText((Object)(_label4));
 //BA.debugLineNum = 157;BA.debugLine="LabelMagnetism.Text 	= label5";
mostCurrent._labelmagnetism.setText((Object)(_label5));
 //BA.debugLineNum = 158;BA.debugLine="LabelRotation.Text 		= label6";
mostCurrent._labelrotation.setText((Object)(_label6));
 //BA.debugLineNum = 160;BA.debugLine="LabelBatteryLevel.Text 	= label7";
mostCurrent._labelbatterylevel.setText((Object)(_label7));
 //BA.debugLineNum = 161;BA.debugLine="LabelBlueDevice.Text 	= label8";
mostCurrent._labelbluedevice.setText((Object)(_label8));
 //BA.debugLineNum = 162;BA.debugLine="LabelBlueConnectStatus.Text = SensorData.BlueConn";
mostCurrent._labelblueconnectstatus.setText((Object)(mostCurrent._sensordata._blueconnectedstatus));
 //BA.debugLineNum = 163;BA.debugLine="LabelDate.Text = \"Today is\"";
mostCurrent._labeldate.setText((Object)("Today is"));
 //BA.debugLineNum = 164;BA.debugLine="LabelTime.Text = DateTime.Date(DateTime.Now)";
mostCurrent._labeltime.setText((Object)(anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow())));
 //BA.debugLineNum = 166;BA.debugLine="ButtonRefresh.Text = SensorData.RefreshButtonStat";
mostCurrent._buttonrefresh.setText((Object)(mostCurrent._sensordata._refreshbuttonstatus));
 //BA.debugLineNum = 168;BA.debugLine="End Sub";
return "";
}
public static String  _selectdeviceandconnect(anywheresoftware.b4a.objects.collections.List _devices) throws Exception{
int _device = 0;
 //BA.debugLineNum = 206;BA.debugLine="Sub SelectDeviceAndConnect(devices As List)	'serve";
 //BA.debugLineNum = 207;BA.debugLine="Log (\"SelectDeviceAndConnect\")";
anywheresoftware.b4a.keywords.Common.Log("SelectDeviceAndConnect");
 //BA.debugLineNum = 208;BA.debugLine="Dim device As Int";
_device = 0;
 //BA.debugLineNum = 209;BA.debugLine="device = InputList(devices, \"Choose device\", -1)";
_device = anywheresoftware.b4a.keywords.Common.InputList(_devices,"Choose device",(int) (-1),mostCurrent.activityBA);
 //BA.debugLineNum = 210;BA.debugLine="If device <> DialogResponse.CANCEL Then";
if (_device!=anywheresoftware.b4a.keywords.Common.DialogResponse.CANCEL) { 
 //BA.debugLineNum = 211;BA.debugLine="BLE2Interface.ConnectedDevice = BLE2Interface.Fo";
mostCurrent._ble2interface._connecteddevice = (com.iprotoxi.blue.bleinterface._nameandmac)(mostCurrent._ble2interface._founddevices.Get(_device));
 //BA.debugLineNum = 212;BA.debugLine="ProgressDialogShow(\"Trying to connect to \" & BLE";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow(mostCurrent.activityBA,"Trying to connect to "+mostCurrent._ble2interface._connecteddevice.Name+" ("+mostCurrent._ble2interface._connecteddevice.MAC+")");
 //BA.debugLineNum = 213;BA.debugLine="If BLE2Interface.ConnectState == BLE2Interface.c";
if (mostCurrent._ble2interface._connectstate==mostCurrent._ble2interface._cstateidle) { 
 //BA.debugLineNum = 214;BA.debugLine="BLE2Interface.Manager.Connect(BLE2Interface.Con";
mostCurrent._ble2interface._manager.Connect(mostCurrent._ble2interface._connecteddevice.MAC);
 }else {
 //BA.debugLineNum = 216;BA.debugLine="ToastMessageShow(\"Connection already active\", T";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Connection already active",anywheresoftware.b4a.keywords.Common.True);
 };
 };
 //BA.debugLineNum = 219;BA.debugLine="End Sub";
return "";
}
public static String  _selectdeviceandconnect_(anywheresoftware.b4a.objects.collections.List _devices) throws Exception{
int _device = 0;
 //BA.debugLineNum = 222;BA.debugLine="Sub SelectDeviceAndConnect_(devices As List)	'serv";
 //BA.debugLineNum = 223;BA.debugLine="Log (\"SelectDeviceAndConnect\")";
anywheresoftware.b4a.keywords.Common.Log("SelectDeviceAndConnect");
 //BA.debugLineNum = 224;BA.debugLine="Dim device As Int";
_device = 0;
 //BA.debugLineNum = 225;BA.debugLine="device = InputList(devices, \"Choose device\", -1)";
_device = anywheresoftware.b4a.keywords.Common.InputList(_devices,"Choose device",(int) (-1),mostCurrent.activityBA);
 //BA.debugLineNum = 226;BA.debugLine="If device <> DialogResponse.CANCEL Then";
if (_device!=anywheresoftware.b4a.keywords.Common.DialogResponse.CANCEL) { 
 //BA.debugLineNum = 227;BA.debugLine="BLEInterface.ConnectedDevice = BLE2Interface.Fou";
mostCurrent._bleinterface._connecteddevice = (com.iprotoxi.blue.bleinterface._nameandmac)(mostCurrent._ble2interface._founddevices.Get(_device));
 //BA.debugLineNum = 228;BA.debugLine="ProgressDialogShow(\"Trying to connect to \" & BLE";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow(mostCurrent.activityBA,"Trying to connect to "+mostCurrent._ble2interface._connecteddevice.Name+" ("+mostCurrent._bleinterface._connecteddevice.MAC+")");
 //BA.debugLineNum = 229;BA.debugLine="If BLEInterface.ConnectState == BLE2Interface.cs";
if (mostCurrent._bleinterface._connectstate==mostCurrent._ble2interface._cstateidle) { 
 //BA.debugLineNum = 230;BA.debugLine="BLEInterface.Manager.Connect(BLE2Interface.Conn";
mostCurrent._bleinterface._manager.Connect(mostCurrent._ble2interface._connecteddevice.MAC,anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 232;BA.debugLine="ToastMessageShow(\"Connection already active\", T";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Connection already active",anywheresoftware.b4a.keywords.Common.True);
 };
 };
 //BA.debugLineNum = 235;BA.debugLine="End Sub";
return "";
}
}
