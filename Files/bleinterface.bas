﻿Type=Service
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Service Attributes 
	#StartAtBoot: False
	'#StartCommandReturnValue: android.app.Service.START_STICKY	
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim BT As BluetoothAdmin
	Dim Manager As BleManager
	Dim BatteryLevelReader As Timer
	Dim BatteryLevelReadFirstTime As Timer
	Dim GATTServices As Map
	
	Dim ConnectState As Int
	Dim cstateIdle As Int = 0
'	Dim cstateOnConnect As Int = 1
	Dim cstateConnect As Int = 2
'	Dim cstateOnDisconnect As Int = 3
'	Dim cstateOnClose As Int = 4
'	Dim cstateOnFinish As Int = 5

	Type NameAndMAC (Name As String, MAC As String)
	Dim ConnectedDevice As NameAndMAC
	Dim FoundDevices As List
	Dim MACList As List
	Dim ConnectToLastFlag As Boolean = False
End Sub

Sub Service_Create
	Try
		FoundDevices.Initialize	
		ConnectedDevice.Initialize
		GATTServices.Initialize
		BT.Initialize("btEvent")
		Manager.Initialize("bleEvent")
		BatteryLevelReader.Initialize("ReadBatteryLevel", 3*60000) 'read n times in a minute
		BatteryLevelReadFirstTime.Initialize("ReadBatteryLevelFirstTime", 2000)
		ConnectState = cstateIdle
	Catch
		Log("Service_Create Error: " & LastException)
		StopService("") 'stop current
		ConnectState = cstateIdle
	End Try
	ConnectState = cstateIdle
End Sub

Sub Service_Start (StartingIntent As Intent)
End Sub

Sub Service_Destroy	
End Sub

Sub bleEvent_DeviceFound (Name As String, MacAddress As String)
	Log("DeviceFound: " & Name & "(" & MacAddress & ")")
	If MacAddress <> Null Then
		Dim nameAndMac As NameAndMAC
		nameAndMac.Initialize
		nameAndMac.Name = Name
		nameAndMac.Mac = MacAddress
		If ConnectToLastFlag = True Then
			If MacAddress = Starter.lastMAC Then Manager.Connect(Starter.lastMAC, True)
		End If
		If MACList.IndexOf(MacAddress) = -1 Then ' only add to list if not duplicate
			MACList.Add(MacAddress)
			FoundDevices.Add(nameAndMac)
			CallSub2(Main, "ProgressDialogShow_", "~ SKIIOT device(s) found...".Replace("~", FoundDevices.Size))
		End If
	End If
End Sub

Sub bleEvent_DiscoveryFinished
	Log("DiscoveryFinished")
	If IsPaused(Main) = False Then CallSub(Main, "ProgressDialogHide_")
	If FoundDevices.Size = 0 Then
		ToastMessageShow("No SKIIOT devices found", False)
	Else
		Dim devices As List
		devices.Initialize
		For i = 0 To FoundDevices.Size - 1
			Dim nameAndMac As NameAndMAC
			nameAndMac = FoundDevices.Get(i)
			devices.Add(nameAndMac.Name)
		Next
		Log("Connecting to " & devices.Get(0))
		If IsPaused(Main) = False Then CallSub2(Main, "SelectDeviceAndConnect", devices)
	End If
End Sub

Sub bleEvent_Connected (Services As Map)
	Log("Connected, " & Services.Size & " services available")
	GATTServices = Services
	BatteryLevelReader.Enabled = True
	BatteryLevelReadFirstTime.Enabled = True
	Dim i, j As Int
'	Dim heartRateServiceP As Int = 2
'	Dim heartRateMeasurementCharacteristicP As Int  = 0
'	Dim bodySensorLocationCharacteristicP As Int = 1	
	Dim nRFUartServiceP As Int = 5
	Dim nRFUartRXCharacteristicP As Int = 0
'	Dim nRFUartTXCharacteristicP As Int = 1
	Try
		If ConnectedDevice.MAC <> Null Then	'ConnectedDevice.Name
			Starter.lastMAC = ConnectedDevice.MAC
			Starter.lastName = ConnectedDevice.Name
			Starter.KVS.PutSimple("MAC", ConnectedDevice.MAC)
			Starter.KVS.PutSimple("name", ConnectedDevice.Name)
		End If
	Catch
		Log("Connected Error1" & LastException)
	End Try	
	If ConnectState == cstateIdle Then
		ConnectState = cstateConnect
		SensorData.SkiiotConnectedStatus = "Connected"
		ToastMessageShow("Connected", False)
		For i = 0 To GATTServices.Size - 1
			Dim workerService As BleService = GATTServices.GetValueAt(i)
			Log ("Service[" & i & "] UUID: " & GATTServices.GetKeyAt(i))	' Log the UUID of the selected service
			For j = 0 To workerService.GetCharacteristics.Size - 1
				Dim workerCharacteristic As BleCharacteristic = workerService.GetCharacteristics.GetValueAt(j)
				Log("Charasteristic[" & j & "] UUID: " & workerCharacteristic.Uuid)
'				Log("Charasteristic[" & j & "] Permissions: " & BleCharacteristicPermissionsToString(workerCharacteristic))
				Log("Charasteristic[" & j & "] Properties: " & BleCharacteristicPropertiesToString(workerCharacteristic))
				Log("Charasteristic[" & j & "] WriteType: " & BleCharacteristicWriteTypeToString(workerCharacteristic))
'				Log(" ")
			Next
		Next
		Try
			Dim NRFUartService As BleService = GATTServices.GetValueAt(nRFUartServiceP)
			Dim ReadCharacteristic As BleCharacteristic = NRFUartService.GetCharacteristics.GetValueAt(nRFUartRXCharacteristicP)
'			Dim WriteCharacteristic As BleCharacteristic = NRFUartService.GetCharacteristics.GetValueAt(nRFUartTXCharacteristicP)
			Manager.ReadCharacteristic(ReadCharacteristic)
			Manager.SetCharacteristicNotification(ReadCharacteristic, True)
		Catch
			'Incompatible Service or Characteristic
			Log ("Connected Error2: " & LastException)
			ToastMessageShow("Incompatible SKIIOT device. Disconnecting...", False)
			DisconnectBLE
		End Try
	End If
	If IsPaused(Main) = False Then CallSub(Main, "ProgressDialogHide_")
End Sub

Sub bleEvent_CharacteristicRead (Success As Boolean, Characteristic As BleCharacteristic)
	If Success Then
		If Characteristic.Uuid == "00002a19-0000-1000-8000-00805f9b34fb" Then 'Battery level characteristic
			Log("CharacteristicRead UUID: " & Characteristic.Uuid & " Value: " & Characteristic.GetValue(0))
			SensorData.currentBattery = Characteristic.GetValue(0)
		End If
	End If
End Sub

Sub bleEvent_CharacteristicChanged (Characteristic As BleCharacteristic)
	Dim ByteConverter As ByteConverter
	Dim value As String		
	Dim timestamp As String =  DateTime.Date(DateTime.Now)
	Dim dataHex As String = ByteConverter.HexFromBytes(Characteristic.GetValue)
'	Log ("dataHex: " & dataHex)
	If Characteristic.Uuid == "6e400003-b5a3-f393-e0a9-e50e24dcca9e" Then 'nRFUart RX characteristic
'		Dim registerHex As String = dataHex.SubString2(12, 16)
		Dim registerHex As String = dataHex.SubString2(0,4)
		Select registerHex	
					
			Case "B328" ' motion data
				
				'accelerometer
				Dim accData As String = ParseData.Parse3axisHexToString(dataHex.SubString2(4, 16))
				value = timestamp & ";a;" & accData
				If Starter.logFlag = True Then 
					If Starter.CSVstore = True Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If Starter.SQLstore = True Then CallSubDelayed2(Starter, "StoreData", value)
				End If

				'magnetometer
				Dim magnData As String = ParseData.Parse3axisHexToString(dataHex.SubString2(16, 28))
				value = timestamp & ";m;" & magnData
				If Starter.logFlag = True Then 
					If Starter.CSVstore = True Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If Starter.SQLstore = True Then CallSubDelayed2(Starter, "StoreData", value)
				End If
				
				'gyroscope
				Dim gyroData As String = ParseData.Parse3axisHexToString(dataHex.SubString2(28, 40))
				value = timestamp & ";g;" & gyroData
				If Starter.logFlag = True Then 
					If Starter.CSVstore = True Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If Starter.SQLstore = True Then CallSubDelayed2(Starter, "StoreData", value)
				End If				

			Case "B168" ' ambient data
				
				'snow temperature
				Dim snowData As String = ParseData.ParseHexToFloat(dataHex.SubString2(22, 26))
				value = timestamp & ";s;" & snowData
				If Starter.logFlag = True Then 
					If Starter.CSVstore = True Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If Starter.SQLstore = True Then CallSubDelayed2(Starter, "StoreData", value)
				End If
				SensorData.currentSnowTemp = ParseData.ParseResultToString(snowData)
				
				'air humidity
				Dim humiData As String = ParseData.ParseHexToFloat(dataHex.SubString2(14, 18))
				value = timestamp & ";h;" & humiData
				If Starter.logFlag = True Then 
					If Starter.CSVstore = True Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If Starter.SQLstore = True Then CallSubDelayed2(Starter, "StoreData", value)
				End If								
				SensorData.currentAirHumidity = ParseData.ParseResultToString(humiData)

				'air temperature
				Dim tempData As String = ParseData.ParseHexToFloat(dataHex.SubString2(10, 14)) '18,22
				value = timestamp & ";t;" & tempData
				If Starter.logFlag = True Then 
					If Starter.CSVstore = True Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If Starter.SQLstore = True Then CallSubDelayed2(Starter, "StoreData", value)
				End If				
				SensorData.currentAirTemp = ParseData.ParseResultToString(tempData)
				
				'air pressure
				Dim baroData As String = ParseData.parseHexToAirPressure(dataHex.SubString2(4, 10))
				value = timestamp & ";b;" & baroData
				If (Starter.logFlag) Then 
					If (Starter.CSVstore) Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If (Starter.SQLstore) Then CallSubDelayed2(Starter, "StoreData", value)
				End If				
				SensorData.currentAirPressure = ParseData.ParseResultToString(baroData)

		End Select
	Else
		If Characteristic.Uuid == "00002a37-0000-1000-8000-00805f9b34fb" Then 
			Log("Surprise! Heart rate found!") 'Heart rate measurement characteristic
			SensorData.currentHeartRateDevice = "device found"
		End If
	End If

End Sub

Sub bleEvent_CharacteristicWrite (Success As Boolean, Characteristic As BleCharacteristic)
	If Characteristic.Uuid == "6e400002-b5a3-f393-e0a9-e50e24dcca9e" Then 'nRFUart TX characteristic
		Log("CharacteristicWrite Success: " & Success & ",  Value After write: " & Characteristic.GetIntValue(Characteristic.FORMAT_UINT8,0))
	End If	
End Sub

Sub bleEvent_Disconnect
	Log ("Disconnect")
	DisconnectBLE
End Sub

Sub ReadBatteryLevelFirstTime_Tick
	Dim batteryServiceP As Int = 3
	Dim batteryLevelCharacteristicP As Int = 0
	Dim BatteryService As BleService = GATTServices.GetValueAt(batteryServiceP)
	Dim BatteryLevelCharacteristic As BleCharacteristic = BatteryService.GetCharacteristics.GetValueAt(batteryLevelCharacteristicP)
	Manager.ReadCharacteristic(BatteryLevelCharacteristic)
	BatteryLevelReadFirstTime.Enabled = False 'disable fast battery level reader
End Sub

Sub ReadBatteryLevel_Tick
	Dim batteryServiceP As Int = 3
	Dim batteryLevelCharacteristicP As Int = 0
	Dim BatteryService As BleService = GATTServices.GetValueAt(batteryServiceP)
	Dim BatteryLevelCharacteristic As BleCharacteristic = BatteryService.GetCharacteristics.GetValueAt(batteryLevelCharacteristicP)
	Manager.ReadCharacteristic(BatteryLevelCharacteristic)
End Sub

Sub DisconnectBLE
	Manager.Disconnect
	Delay(250, True)
	Manager.Close
	ConnectState = cstateIdle
	ToastMessageShow("Disconnected", False)
	SensorData.SkiiotConnectedStatus = "Disconnected"
	SensorData.currentSnowTemp = "----"
	SensorData.currentAirTemp = "----"	
	SensorData.currentAirHumidity = "----"
	SensorData.currentAirPressure = "----"
	SensorData.currentAccX = "----"
	SensorData.currentAccY = "----"
	SensorData.currentAccZ = "----"
	SensorData.currentMagX = "----"
	SensorData.currentMagY = "----"
	SensorData.currentMagZ = "----"
	SensorData.currentGyroX = "----"
	SensorData.currentGyroY = "----"
	SensorData.currentGyroZ = "----"
	SensorData.currentBattery = "----"
	SensorData.counter = 0
End Sub

Sub ConnectToDevice
	ToastMessageShow("Searching for SKIIOT devices...", False)
	FoundDevices.Initialize
	MACList.Initialize
	Dim serviceUUIDs(6) As String
	serviceUUIDs(0) = "00001800-0000-1000-8000-00805f9b34fb" 'Generic Access Service
	serviceUUIDs(1) = "00001801-0000-1000-8000-00805f9b34fb" 'Generic Attribute Service
	serviceUUIDs(2) = "0000180d-0000-1000-8000-00805f9b34fb" 'Heart Rate Service
	serviceUUIDs(3) = "0000180f-0000-1000-8000-00805f9b34fb" 'Battery Service
	serviceUUIDs(4) = "0000180a-0000-1000-8000-00805f9b34fb" 'Device Information Service
	serviceUUIDs(5) = "6e400001-b5a3-f393-e0a9-e50e24dcca9e" 'nRFUart Service
	Try
		If BT.IsEnabled = False Then
			BT.Enable
			Delay(2000, True)
		End If
		ConnectState = cstateIdle
		If (Manager.BleSupported) Then
			Manager.Scan(8000, Null) ' scan to discover all devices
		Else
			DisconnectBLE
			ToastMessageShow("No SKIIOT devices found", True)
		End If
	Catch
		Log("ConnectToDevice Error" & LastException)
	End Try
End Sub

Sub Delay (nMilliSecond As Long, EnableEvents As Boolean)
	Dim nBeginTime, nEndTime As Long
	nEndTime = DateTime.Now + nMilliSecond
	nBeginTime = DateTime.Now
	Do While nBeginTime < nEndTime
		nBeginTime = DateTime.Now
		If nEndTime < nBeginTime Then Return
		If (EnableEvents) Then DoEvents
	Loop
End Sub

' Helper function to dump the properties of the connection
Sub BleCharacteristicPermissionsToString (Characteristic As BleCharacteristic) As String
	Dim Output As StringBuilder
	Output.Initialize
	
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_READ) <> 0 Then
		Output.Append("Read ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_READ_ENCRYPTED) <> 0 Then
		Output.Append("READ_ENCRYPTED ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_READ_ENCRYPTED_MITM)  <> 0 Then
		Output.Append("Read_E_MITM ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE) <> 0 Then
		Output.Append("Write ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE_ENCRYPTED)  <> 0 Then
		Output.Append("Write_ENCRYPTED ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE) <> 0 Then
		Output.Append("Write_E_MITM ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE_SIGNED) <> 0 Then
		Output.Append("Write_SIGNED ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE_SIGNED_MITM) <> 0 Then
		Output.Append("Write_S_MITM ")
	End If

   Return Output.ToString

End Sub
' Helper function to dump the properties of the connection
Sub BleCharacteristicPropertiesToString (Characteristic As BleCharacteristic) As String
	Dim Output As StringBuilder
	Output.Initialize
	
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_BROADCAST) <> 0 Then
		Output.Append("BROADCAST ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_EXTENDED_PROPS) <> 0 Then
		Output.Append("EXTENDED_PROPS ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_INDICATE) <> 0 Then
		Output.Append("EXTENDED_INDICATE ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_NOTIFY) <> 0 Then
		Output.Append("NOTIFY ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_READ) <> 0 Then
		Output.Append("READ ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_SIGNED_WRITE) <> 0 Then
		Output.Append("SIGNED_WRITE ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_WRITE) <> 0 Then
		Output.Append("WRITE ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_WRITE_NO_RESPONSE) <> 0 Then
		Output.Append("WRITE_NO_RESPONSE ")
	End If
	
	Return Output.ToString
	
End Sub
' Helper function to dump the properties of the connection
Sub BleCharacteristicWriteTypeToString (Characteristic As BleCharacteristic) As String
	Dim Output As StringBuilder
	Output.Initialize
	
	If Bit.And(Characteristic.WriteType , Characteristic.WRITE_TYPE_DEFAULT) <> 0 Then
		Output.Append("TYPE_DEFAULT ")
	End If
	If Bit.And(Characteristic.WriteType , Characteristic.WRITE_TYPE_NO_RESPONSE) <> 0 Then
		Output.Append("TYPE_NO_RESPONSE ")
	End If
	If Bit.And(Characteristic.WriteType , Characteristic.WRITE_TYPE_SIGNED) <> 0 Then
		Output.Append("TYPE_SIGNED ")
	End If

   Return Output.ToString
End Sub