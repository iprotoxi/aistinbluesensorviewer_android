﻿Type=StaticCode
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
End Sub

Sub parseHexToDate(data As String) As String 'for epoch date format
	' data format is 5560A1B4
	Dim longVal, intVal1, intVal2 As Long
	intVal1 = Bit.ParseInt(data.SubString2(0,4), 16)
	intVal2 = Bit.ParseInt(data.SubString(4), 16)
	longVal = intVal1 * 65536 + intVal2	
	'Convert to date string
	Dim timeText As String
	DateTime.TimeFormat = "yyyy-MM-dd hh:mm:ss:SS"
	timeText = DateTime.Time(longVal * 1000)
	Return timeText
End Sub

Sub parseHexToFloat (data As String) As Float ' this is for the 8.8 type data
	Dim tempVal As Int
	tempVal = Bit.ParseInt(data.SubString2(0,2), 16) * 256 + Bit.ParseInt(data.SubString2(2,4), 16)
	If tempVal >= 32768 Then tempVal = tempVal - 65536
	Dim floatVal As Float
	floatVal = tempVal/256
	Return floatVal
	'Return Round2(floatVal, 2)
End Sub

Sub parseHexTo16bit (data As String) As Float ' this is for the 8.8 type data
	 Dim tempVal As Int
	 tempVal = Bit.ParseInt(data, 16) '* 256  + Bit.ParseInt(data.SubString2(2,4), 16)
	 'If tempVal >= 32768 Then tempVal = tempVal - 65536
	 Dim floatVal As Float
	 floatVal = tempVal
	 Return floatVal
End Sub

Sub parseHexToAirPressure (data As String) As Float
	Dim val0, val1, val2 As Long
	val0 = Bit.ParseInt(data.SubString2(0,2), 16)
	val1 = Bit.ParseInt(data.SubString2(2,4), 16)
	val2 = Bit.ParseInt(data.SubString2(4,6), 16)
	Dim floatVal As Float
	floatVal = val0*16 + val1/16 + val2/4096
	Dim returnval As Float = Round2(floatVal, 2)
	Return returnval
End Sub

Sub ParseResultToString (data As Float) As String
	Dim sensorvalString, result As String
	Dim decimalSeparator As Int
	sensorvalString = data
	decimalSeparator = sensorvalString.IndexOf(".")
	If sensorvalString.Length > decimalSeparator + 3 Then
		result = sensorvalString.SubString2(0, decimalSeparator + 3)
	Else
		result = sensorvalString
	End If
	Return result
End Sub

Sub Parse3axisHexToString(dataHex As String) As String
	Dim	xHex As String = dataHex.SubString2(0, 4)
	Dim	yHex As String = dataHex.SubString2(4, 8)
	Dim	zHex As String = dataHex.SubString2(8, 12)
	Dim retval As String = 	parseHexToFloat(xHex) & ";" & parseHexToFloat(yHex) & ";" & parseHexToFloat(zHex)
	Return retval
End Sub
