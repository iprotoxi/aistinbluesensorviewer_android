﻿Type=Service
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Service Attributes 
	#StartAtBoot: False
	' THIS SERVICE MODULE USES BLE2 LIBRARY
	' THIS SERVICE MODULE IS CURRENTLY IN USE IN THIS PROJECT
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim Manager As BleManager2
	
	Dim GATTServicesList As List
	
	Private GenericAccessService As String 		= "00001800-0000-1000-8000-00805f9b34fb"
	Private GenericAttributeService As String 	= "00001801-0000-1000-8000-00805f9b34fb"
	Private HeartRateService As String 			= "0000180d-0000-1000-8000-00805f9b34fb"
	Private BatteryService As String 			= "0000180f-0000-1000-8000-00805f9b34fb"
	Private DeviceInformationService As String 	= "0000180a-0000-1000-8000-00805f9b34fb"
	Private DFUService As String 				= "00001530-1212-efde-1523-785feabcd123"
	Private nRFUartService As String 			= "6e400001-b5a3-f393-e0a9-e50e24dcca9e"
	
	Private nRFUartRXcharacteristic As String 	= "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
	Private nRFUartTXcharacteristic As String 	= "6e400003-b5a3-f393-e0a9-e50e24dcca9e"
	
	Private BatteryLevelCharacteristic As String = "00002a19-0000-1000-8000-00805f9b34fb"
		
	Private startLoggingMessage() As Byte = Array As Byte (0x04, 0x01, 0x01, 0x86, 0x07, 0x03)
	Private stopLoggingMessage()  As Byte = Array As Byte (0x04, 0x01, 0x01, 0x86, 0x07, 0x00)
	
	Dim BatteryLevelReader As Timer
	
'	Type NameAndMAC (Name As String, MAC As String)
	Dim FoundDevices As List
	Dim MACList As List
	Dim ConnectedDevice As NameAndMAC	
	Dim ConnectToLastFlag As Boolean

	Private ScanStopTimer As Timer	
	
	Dim isRealtimeData As Boolean
	Dim recordedPacket As Int
	
	Dim errorFlag As Boolean
	
	Dim ConnectState As Int
	Dim cstateIdle As Int = 0
'	Dim cstateOnConnect As Int = 1
	Dim cstateConnect As Int = 2
'	Dim cstateOnDisconnect As Int = 3
'	Dim cstateOnClose As Int = 4
'	Dim cstateOnFinish As Int = 5

End Sub

Sub Service_Create
	ScanStopTimer.Initialize("ScanStopTimer", 4*1000)
	MACList.Initialize
	FoundDevices.Initialize	
	ConnectedDevice.Initialize	
	GATTServicesList.Initialize	
	Manager.Initialize("BLE_Event")
	BatteryLevelReader.Initialize("ReadBatteryLevel", 0.6*60*1000) 'read once every n minutes
	ConnectState = cstateIdle	
End Sub

Sub Service_Start (StartingIntent As Intent)
	ConnectToLastFlag = True
	isRealtimeData = True
	errorFlag = False
End Sub

Sub Service_Destroy
	CallSub(Starter, "CloseTextwriter")
	StopService("") 'stop self
End Sub

#Region BLE2 EVENTS
	Sub BLE_Event_StateChanged (State As Int)
		Select State
			Case Manager.STATE_UNSUPPORTED
				Log("BLE_Event_StateChanged, Manager.STATE_UNSUPPORTED: " & State)				
			Case Manager.STATE_POWERED_OFF
				Log("BLE_Event_StateChanged, Manager.STATE_POWERED_OFF: " & State)
			Case Manager.STATE_POWERED_ON
				Log("BLE_Event_StateChanged, Manager.STATE_POWERED_ON: " & State)
		End Select
	End Sub

	Sub BLE_Event_DeviceFound (Name As String, DeviceId As String, AdvertisingData As Map, RSSI As Double)
		Log("BLE_Event_DeviceFound, Name: " & Name & ", DeviceId: " & DeviceId & ", AdvertisingData: " & AdvertisingData & " RSSI: " & RSSI)
'		Dim bc As ByteConverter
'		For Each Key As Int In AdvertisingData.Keys
' 			Dim b() As Byte = AdvertisingData.Get(Key)
'	 		Log("Key " & Key & ": " & b &" : " & bc.HexFromBytes(b) & " : " & bc.StringFromBytes(b, "UTF8"))
'		Next
'		Log("--")		
		If IsPaused(Main) = False Then CallSub(Main, "ProgressDialogHide_")
		If DeviceId <> Null Then
			If Name.Contains("BTL") Then				
				Dim nameAndMac As NameAndMAC
				nameAndMac.Initialize
				nameAndMac.Name = Name
				nameAndMac.Mac = DeviceId					
				MACList.Add(DeviceId)
				FoundDevices.Add(nameAndMac)
				Log("Found devices: " & FoundDevices.Size)
				If IsPaused(Main) = False Then CallSub2(Main, "ProgressDialogShow_", "~ device(s) found...".Replace("~", FoundDevices.Size))
				Log("ConnectToLastFlag: " & ConnectToLastFlag)				
				If (ConnectToLastFlag) Then 
					If DeviceId = Starter.lastConnectedMAC Then 
						Manager.Connect(DeviceId)
						Manager.StopScan				
						ScanStopTimer.Enabled = False
						ConnectedDevice.MAC = DeviceId
						ConnectedDevice.Name = Name
					End If
				End If				
			End If
		End If
	End Sub

	Sub BLE_Event_Disconnected
		Log("BLE_Event_Disconnected")
		ConnectState = cstateIdle		
		BatteryLevelReader.Enabled = False
		SensorData.ConnectedDeviceName = "Aistin Blue"
		SensorData.BlueConnectedStatus = "Disconnected"
		SensorData.RefreshButtonStatus = "Scan"	
		SensorData.currentAirTemp = "---"	
		SensorData.currentAirHumidity = "---"
		SensorData.currentAirPressure = "---"
		SensorData.currentAcceleration = "---"
		SensorData.currentMagnetism = "---"
		SensorData.currentRotation = "---"
		SensorData.currentBattery = "---"
		ToastMessageShow("Disconnected", False)			
	End Sub

	Sub BLE_Event_Connected (Services As List)
		Log("BLE_Event_Connected, " & Services.Size & " services discovered")
		GATTServicesList = Services
		If ConnectState == cstateIdle Then
			ConnectState = cstateConnect
			For Each services_ As String In GATTServicesList
				Log("Manager.ReadData(services_)" & services_)
        		Manager.ReadData(services_)
		    Next
		End If
		Try
			Log("ConnectedDevice.MAC: " & ConnectedDevice.MAC & ", ConnectedDevice.Name: " & ConnectedDevice.Name)
			Starter.lastConnectedMAC = ConnectedDevice.MAC
			Starter.lastConnectedName = ConnectedDevice.Name
			Starter.APPData.PutSimple("MAC", ConnectedDevice.MAC)
			Starter.APPData.PutSimple("name", ConnectedDevice.Name)
		Catch
			Log("BLE_Event_Connected Error1" & LastException)
		End Try
		Try
			CallSub(Starter, "CreateLogFile")
		Catch
			Log("Connected Error2" & LastException)
		End Try		
		If IsPaused(Main) = False Then CallSubDelayed(Main, "ProgressDialogHide_")
		Log("Manager.SetNotify(nRFUartService, nRFUartTXcharacteristic, True)")
		SensorData.RefreshButtonStatus = "Rescan"
		SensorData.BlueConnectedStatus = "Connected"
		SensorData.ConnectedDeviceName = Starter.lastConnectedName 'ConnectedDevice.Name		
		Delay(500, True)		
		Manager.SetNotify(nRFUartService, nRFUartTXcharacteristic, True) 'read from uart		
		Delay(500, True)
		Manager.SetNotify(nRFUartService, nRFUartTXcharacteristic, True)		
		'Manager.ReadData2(nRFUartService, nRFUartTXcharacteristic)
		'ToastMessageShow("Connected", False)
		BatteryLevelReader.Enabled = True	
		recordedPacket = 0	
	End Sub

	Sub BLE_Event_DataAvailable (ServiceId As String, Characteristics As Map)
'		Log("BLE_Event_DataAvailable, ServiceId: " & ServiceId)
		Dim convert As ByteConverter
		
		Select ServiceId
		Case GenericAccessService
			Log("BLE_Event_DataAvailable GenericAccessService")
		Case GenericAttributeService
			Log("BLE_Event_DataAvailable GenericAttributeService")
		Case DeviceInformationService
			Log("BLE_Event_DataAvailable DeviceInformationService")			
		Case HeartRateService
			Log("BLE_Event_DataAvailable HeartRateService")
		Case BatteryService
			Log("BLE_Event_DataAvailable BatteryService")
			'Log("batterylevel: " & convert.HexFromBytes( Characteristics.Get(BatteryLevelCharacteristic) ))
			Try
				SensorData.currentBattery = "" & Bit.ParseInt( convert.HexFromBytes( Characteristics.Get(BatteryLevelCharacteristic)), 16 )
			Catch
				Log("BLE_Event_DataAvailable BatteryService: " & LastException)
				errorFlag = True
				Log ("errorFlag = " & errorFlag )
			End Try
		Case DFUService
			Log("BLE_Event_DataAvailable DFUService")
		Case nRFUartService
			'Log("BLE_Event_DataAvailable nRFUartService")
			'Log("nRFUartTXcharacteristic: " & Characteristics.Get(nRFUartTXcharacteristic))
			Dim value As String
			Dim timestamp As String = DateTime.Date(DateTime.Now)
			Dim dataHex As String = convert.HexFromBytes( Characteristics.Get(nRFUartTXcharacteristic) )
			'Log("dataHex: " & dataHex)	
			If dataHex <> "" Then
				Dim sequence As String = dataHex.SubString2(0, 2)
				If sequence.IndexOf("8") = 0 Or sequence.IndexOf("9") = 0 Then
					'If recordedPacket = 0 Then CallSub(Starter, "CreateRecordedLogFile")
					isRealtimeData = False
					recordedPacket = recordedPacket + 1
				Else
					'If recordedPacket <> 0 Then CallSubDelayed(Starter, "CloseTextwriters2")
					isRealtimeData = True					
					recordedPacket = 0
				End If		
				''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				Dim register As String = dataHex.SubString2(2, 4)				
				Select register
				Case "F1" 'motion
					'accelerometer
					Dim accXYZ As String = ParseData.AccelerationToCSV(dataHex.SubString2(4, 13))
					SensorData.currentAcceleration = ParseData.ParseResultToString2(accXYZ)
					'magnetometer
					Dim magXYZ As String = ParseData.MagnetismToCSV(dataHex.SubString2(14, 23))
					SensorData.currentMagnetism = ParseData.ParseResultToString2(magXYZ)
					'gyroscope
					Dim gyrXYZ As String = ParseData.RotationToCSV(dataHex.SubString2(24, 33))
					SensorData.currentRotation = ParseData.ParseResultToString2(gyrXYZ)
					'log
					If (Starter.logFlag) Then 
						value = timestamp &";"& sequence &";"& register &";"& accXYZ &";"& magXYZ &";"& gyrXYZ
						If (Starter.CSVstore) Then CallSubDelayed2(Starter, "WriteDataToFile", value)
						If (Starter.SQLstore) Then CallSubDelayed2(Starter, "StoreData", value)
					End If
				
				Case "F2" 'ambient
					'air pressure
					Dim baro As String = ParseData.parseHexToAirPressure2(dataHex.SubString2(4, 8))
					SensorData.currentAirPressure = ParseData.ParseResultToString(baro)
					'air temperature (from barometer)
					Dim tempB As String = ParseData.ParseHexToFloat(dataHex.SubString2(8, 12))
					SensorData.currentAirTemp = ParseData.ParseResultToString(tempB)
					If dataHex.Length > 19 Then 'BTL_3X1 type of data
						'air humidity
						Dim humi As String = ParseData.ParseHexToFloat(dataHex.SubString2(12, 16))
						SensorData.currentAirHumidity = ParseData.ParseResultToString(humi)
						'air temperature (from humidity sensor)
						'Dim tempH As String = ParseData.ParseHexToFloat(dataHex.SubString2(16, 20))
						'SensorData.currentAirTemp = ParseData.ParseResultToString(tempH)
						value = timestamp &";"& sequence &";"& register &";"& baro &";"& tempB &";"& humi '&";"& tempH
					Else
						value = timestamp &";"& sequence &";"& register &";"& baro &";"& tempB '&";"& humi '&";"& tempH
					End If
					'log
					If (Starter.logFlag) Then
						If (Starter.CSVstore) Then CallSubDelayed2(Starter, "WriteDataToFile", value)
						If (Starter.SQLstore) Then CallSubDelayed2(Starter, "StoreData", value)
					End If		
				End Select
				''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Else
				Log("dataHex: null")	
				If (errorFlag) Then
					DisconnectBLE
					ToastMessageShow("Error in Battery Service after connection", False)
					errorFlag = False
				End If
			End If
		Case Else
			Log("Something else")
		End Select			
	End Sub
#End Region

Sub ScanForDevices 'ConnectToDevice
	ToastMessageShow("Searching for devices...", False)	
	ScanStopTimer.Enabled = True
	FoundDevices.Clear
	Manager.Scan2(Null, False)
End Sub

Sub WriteBytesStartLogging()
    'Dim javaObject As JavaObject = nRFUartRXcharacteristic
    'javaObject.RunMethod("setValue", Array As Object(startLoggingMessage))
	Manager.WriteData(nRFUartService, nRFUartRXcharacteristic, startLoggingMessage)
End Sub

Sub WriteBytesStopLogging()
    'Dim javaObject As JavaObject = nRFUartRXcharacteristic
    'javaObject.RunMethod("setValue", Array As Object(stopLoggingMessage))
	Manager.WriteData(nRFUartService, nRFUartRXcharacteristic, stopLoggingMessage)
End Sub

Sub DisconnectBLE
	Try
		Manager.Disconnect
		ConnectState = cstateIdle
	Catch
		Log("DisconnectBLE Error: " & LastException)
		StopService("") 'stop self
	End Try	
End Sub

Sub ScanStopTimer_Tick
	Log("ScanStopTimer_Tick")
	ScanStopTimer.Enabled = False
	Manager.StopScan
	If IsPaused(Main) = False Then CallSub(Main, "ProgressDialogHide_")
	'DISCOVERY FINISHED -> timer event disabled
	If FoundDevices.Size == 0 Then
		ToastMessageShow("No new devices found", False)
	Else
		Dim devices As List
		devices.Initialize
		For i = 0 To FoundDevices.Size - 1
			Dim nameAndMac As NameAndMAC
			nameAndMac = FoundDevices.Get(i)
			devices.Add(nameAndMac.Name & " (" & nameAndMac.Mac & ")")
		Next
		If IsPaused(Main) = False Then CallSub2(Main, "SelectDeviceAndConnect", devices)
		FoundDevices.Clear
		devices.Clear
	End If
End Sub

Sub ReadBatteryLevel_Tick
'	Log("ReadBatteryLevel_Tick")
	Manager.ReadData2(BatteryService, BatteryLevelCharacteristic)
End Sub

Sub Delay (nMilliSecond As Long, EnableEvents As Boolean)
	Dim nBeginTime, nEndTime As Long
	nEndTime = DateTime.Now + nMilliSecond
	nBeginTime = DateTime.Now
	Do While nBeginTime < nEndTime
		nBeginTime = DateTime.Now
		If nEndTime < nBeginTime Then Return
		If (EnableEvents) Then DoEvents
	Loop
End Sub